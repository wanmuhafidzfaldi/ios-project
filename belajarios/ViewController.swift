//
//  ViewController.swift
//  belajarios
//
//  Created by MacBook Pro on 22/03/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var randomDice1 : Int = 0
    var randomDice2 : Int = 0
    
    @IBOutlet weak var diceImageView1: UIImageView!
    @IBOutlet weak var diceImageView2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func rollDice(_ sender: UIButton) {
        randomDice1 = Int.random(in: 0 ... 5)
        randomDice2 = Int.random(in: 0 ... 5)
        print(randomDice1)
        print(randomDice2)
    }
    
}

